'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.post('/login', 'UserController.login')
Route.post('/chamado', 'OrderController.store').middleware('auth')
Route.get('/cepEnderecoGPS', 'OrderController.validaCep').middleware('auth')
Route.get('/verificarEntrega', 'OrderController.verificaEntrega').middleware('auth')
Route.get('/liberaEntregaV2', 'OrderController.liberaEntrega').middleware('auth')

Route.get('/lastOrder', 'OrderController.index')