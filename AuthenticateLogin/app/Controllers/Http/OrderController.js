'use strict'
const Order = use('App/Models/Order')
const Deliveryman = use('App/Models/Deliveryman')
const Store = use('App/Models/Store')
const axios = use('axios')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with orders
 */

class OrderController {

    async getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = (lat2 - lat1) * (Math.PI / 180); // deg2rad below
        var dLon = (lon2 - lon1) * (Math.PI / 180);
        var a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos((lat1) * (Math.PI / 180)) * Math.cos((lat2) * (Math.PI / 180)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance in km
        return d;
    }

    async liberaEntrega({ request, response }) {

        const req = request.collect(['pedido'])
        var orders = [req.length]
        var deliverymans = [orders.length]

        for (let index = 0; index < req.length; index++) {
            const cod = req[index].pedido.pedido
            orders[index] = await Order.findBy('code', cod)
            deliverymans[index] = await Deliveryman.find(orders[index].deliveryman_id)
        }
        return response.json({
            success: true,
            pedido: [{
                pedido: orders.code,
                success: true,
            }]
        })

    }

    async verificaEntrega({ request, response }) {

        try {

            var status = null
            var type = null
            const req = request.only(['codigo'])

            const order = await Order.findBy('code', req.codigo)
            const deliveryman = await Deliveryman.find(order.deliveryman_id)
            const store = await Store.find(order.store_id)
                // CHAMA O METODO RESPONSÁVEL POR CALCULAR A DISTANCIA ENTRE DOIS PONTOS 
            const distance = this.getDistanceFromLatLonInKm(store.latitude, store.longitude, order.latitude, order.longitude)
            console.log(distance)

            if (order.status_id <= 2) { status = 'ORDER_AWAITING_DELIVERYMAN' }
            if (order.status_id >= 3 && order.status_id <= 7) { status = 'ORDER_DELIVERY_IN_PROGRESS' }
            if (order.status_id >= 8) { status = 'ORDER_DELIVERED' }

            if (order.kind === 'pedido') { type = 'p' } else { type = 's' }
            const data = new Date(order.created_at)
            console.log(data.time)
            console.log(data.hours)
            console.log(data.date)

            return response.json({
                sucess: true,
                status: status,
                entregador: {
                    cpf: deliveryman.cpf,
                    nome: deliveryman.name
                },
                dados: {
                    id: order.code,
                    type: type,
                    date: order.created_at,
                    time: order.created_at,
                    value: 0,
                    distance: order.distance,
                    latitude: order.latitude,
                    longitude: order.longitude
                },
                data: new Date()

            })

        } catch (error) {

            response.json({
                error: {
                    code: error.code,
                    field: error.field,
                    message: error.message
                }
            })
        }
    }


    async validaCep({ request, response }) {

            try {

                const req = request.only(['logradouro', 'bairro', 'cidade', 'logradouro-tipo', 'cep', 'uf'])


                if (!req.logradouro) {
                    throw new Error('Logradouro obrigatório')
                }
                if (!req.bairro) {
                    throw new Error('Bairro obrigatório')
                }
                if (!req.cidade) {
                    throw new Error('Cidade obrigatório')
                }
                if (!req.uf) {
                    throw new Error('Uf obrigatório')
                }

                var resp = null
                if (req.cep) { //PRIMEIRO VERIFICA PELO CEP

                    req.cep = req.cep.replace("-", "").replace(".", "") // FAZ REPLACE NO CEP RETIRANDO CARACTERES
                    resp = await axios.get(`https://www.cepaberto.com/api/v3/cep?cep=${req.cep}`, {
                        headers: {
                            'Authorization': 'Token token=cc19e544424caa97cff63730a5bbe800'
                        }
                    })
                }

                if (resp === null || Object.values(resp.data).length === 0) { //CASO A BUSCA PELO CEP NÃO DE RETORNO 

                    const resp2 = await axios.get(`https://www.cepaberto.com/api/v3/address`, { // VERIFICA PELO RESTO DO ENDEREÇO
                        headers: { 'Authorization': 'Token token=cc19e544424caa97cff63730a5bbe800' },
                        params: { 'estado': req.uf, 'cidade': req.cidade, 'bairro': req.bairro, 'logradouro': req.logradouro }
                    })

                    if (Object.values(resp2.data).length === 0) {
                        throw new Error('Dados informados não são válidos')
                    }

                    return response.json({
                        success: true,
                        gps: {
                            lat: resp2.data.latitude,
                            lng: resp2.data.longitude
                        }
                    })

                }

                return response.json({
                    success: true,
                    gps: {
                        lat: resp.data.latitude,
                        lng: resp.data.longitude
                    }
                })

            } catch (error) {

                return response.json({
                    success: false,
                    error: {
                        code: error.code,
                        message: error.message
                    }
                })

            }

        }
        /**
         * Show a list of all orders.
         * GET orders
         *
         * @param {object} ctx
         * @param {Request} ctx.request
         * @param {Response} ctx.response
         * @param {View} ctx.view
         */
    async index({ request, response }) {
        const ultimoInserido = await Order.query().last()
        return ultimoInserido
    }

    /**
     * Render a form to be used for creating a new order.
     * GET orders/create
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     * @param {View} ctx.view
     */
    async create({ request, response, view }) {}

    /**
     * Create/save a new order.
     * POST orders
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */

    async store({ request, response }) {
        try {

            var currentTime = new Date();
            const orderRequest = request.only(['pedido', 'cliente', 'endereco', 'cep', 'retorno', 'receita'])
            orderRequest.cep = orderRequest.cep.replace("-", "").replace(".", "") // faz replace  e retira . e - do cep 

            const resp = await axios.get(`https://www.cepaberto.com/api/v3/cep?cep=${orderRequest.cep}`, {
                headers: {
                    'Authorization': 'Token token=cc19e544424caa97cff63730a5bbe800'
                }
            })
            if (Object.values(resp.data).length === 0) { // verifica se o objeto resultado da resposta da api cep aberto é vazio
                throw new Error("cep invalido")
            }

            const orderSet = {
                code: orderRequest.pedido,
                creation_date: currentTime,
                status_id: 1,
                store_id: 2,
                client: orderRequest.cliente,
                address: orderRequest.endereco,
                neighborhood: "",
                city_id: 1,
                deliveryman_id: null,
                delivered_at: null,
                latitude: resp.data.latitude,
                longitude: resp.data.longitude,
                created_at: currentTime,
                updated_at: currentTime,
                delivery_commission: 0,
                accepted_at: currentTime,
                time_deliver: null,
                user_id: null,
                obs: null,
                order_cancel_id: null,
                kind: 'pedido',
                delivery_done: 'cadastrado',
                value: 100,
                payment_type: 10,
                status_delivery: null,
                distance: null,
                distributed_at: null,
                delivered_latitude: null,
                delivered_longitude: null,
                password: null,
                place_latitude: null,
                place_longitude: null,
                data: null,
                race_id: null
            }
            const order = await Order.create(orderSet)

            return response.json({
                sucess: 'true',
                chamado: order.id,
                data: order.creation_date,
                status: order.status_id

            })

        } catch (error) {
            response.json({
                error: {
                    message: error.message,
                },
            })

        }
    }

    /**
     * Display a single order.
     * GET orders/:id
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     * @param {View} ctx.view
     */
    async show({ params, request, response, view }) {}

    /**
     * Render a form to update an existing order.
     * GET orders/:id/edit
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     * @param {View} ctx.view
     */
    async edit({ params, request, response, view }) {}

    /**
     * Update order details.
     * PUT or PATCH orders/:id
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async update({ params, request, response }) {}

    /**
     * Delete a order with id.
     * DELETE orders/:id
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async destroy({ params, request, response }) {}
}

module.exports = OrderController