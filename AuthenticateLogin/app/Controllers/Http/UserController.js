'use strict'
const User = use('App/Models/User')
class UserController {
    async login({ request, response, auth }) {
        try {
            const { email } = request.all()

            const userBD = await User.findBy('email', email) //Verificar se email existe no BD

            if (!userBD) { //Caso email não seja cadastrato lança error
                throw new Error();
            }

            const token = await auth.generate(userBD, {
                    permission: userBD.permission
                }) //Caso exista gera um token para o usuario 

            return response.json({
                status: 'true',
                menssagem: await token
            })

        } catch (error) {
            return response.json({ status: 'false', menssagem: 'E-mail/Senha inválido' })
        }
    }
}

module.exports = UserController