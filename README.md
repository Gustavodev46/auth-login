# API
Criação de API utilizando node.js

### 🛠 Tecnologias

As seguintes ferramentas foram usadas na construção do projeto:

- [Node.js](https://nodejs.org/en/)
- [TypeScript](https://www.typescriptlang.org/)
- [Adonis](https://adonisjs.com/)

## Regras

- Autenticação do usuário

  - [x] Só é permitido de autenticar e receber o token usuários com email cadastrado no sistema


### 🎲 Rodando o Back End (servidor)

```bash
# Clone este repositório
$ git clone <https://Gustavodev46@bitbucket.org/Gustavodev46/auth-login.git>

# Acesse a pasta do repositório no terminal/cmd
$ cd auth-login

# Navegue até a pasta do projeto
$ cd AuthenticateLogin

# Instale as dependências
$ yarn install || npm install

# Execute a aplicação em modo de desenvolvimento
$ adonis serve --dev

# O servidor inciará na porta:3333 - acesse <http://127.0.0.1:3333/>
```
É recomendado que a requisições sejam realizadas por um consumidor de API como:
- [Insomnia](https://insomnia.rest/download)
- [Postman](https://www.postman.com/downloads/)
## Endpoints para acesso das rotas criadas
* ```http://127.0.0.1:3333/api/v1/auth/login``` para autenticar um usuário 
/ Estrutura do Json =>  
  { 
	"email": ""
} 
